package ssm.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * 分页结果集
 * 
 * @author lzp
 * @Date 2020-9-4 8:46:44
 */
public class PageInfo<T> implements Serializable {
	private List<T> rows;// 数据集
	private long total;// 数据总条数
	private int pageSize=5;
	private int offset;// 记录开始行
	private int page;// 记录当前页
	private String sort;// 排序关键词
	private String sortOrder;// 排序方式
	private String keyWord = "";// 排序方式

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	@Override
	public String toString() {
		return "PageInfo [rows=" + rows + ", total=" + total + ", pageSize=" + pageSize + ", offset=" + offset
				+ ", page=" + page + ", sort=" + sort + ", sortOrder=" + sortOrder + ", keyWord=" + keyWord + "]";
	}
	
}
