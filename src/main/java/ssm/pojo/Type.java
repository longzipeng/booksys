package ssm.pojo;

import ssm.utils.DateUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * 书本类型
 * 
 * @author Bacon 2020年9月3日
 */
public class Type implements Serializable {
	private int id;
	private String name;
	private Date createTime;
	private String createTimeStr;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateTimeStr() {
		if (createTime != null) {
			createTimeStr = DateUtils.utilDateToStr(createTime, "yyyy-MM-dd HH:mm:ss");
		}
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

}
