package ssm.pojo;

import ssm.utils.DateUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户类
 * @author Zhoux
 * @Date 2020年9月3日
 */
public class User implements Serializable{
	private int id;
	private String username;
	private String password;
	private String name;
	private double money;
	private String email;
	private Date createTime;
	private String createTimeStr;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String getCreateTimeStr() {
		if(createTime!=null) {
			createTimeStr = DateUtils.utilDateToStr(createTime, "yyyy-MM-dd HH:mm:ss");
		}
		return createTimeStr;
	}
	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}
}
