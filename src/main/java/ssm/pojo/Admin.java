package ssm.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * 管理员类
 * 
 * @author Zhoux
 * @Date 2020年9月3日
 */
public class Admin implements Serializable {
	private int id;
	private String username;
	private String password;
	private String name;
	private Date createTime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "Admin [id=" + id + ", username=" + username + ", password=" + password + ", name=" + name
				+ ", createTime=" + createTime + "]";
	}

}
