package ssm.pojo;

import java.io.Serializable;

/**
 * 通用结果集
 * 
 * @author lzp
 * @Date 2020-9-6 23:43:08
 */
public class Result implements Serializable {
	private boolean flag;
	private String msg;

	public boolean getFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
