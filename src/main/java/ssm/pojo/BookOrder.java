package ssm.pojo;

import ssm.utils.DateUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单类
 * 
 * @author Bacon 2020年9月3日
 */
public class BookOrder implements Serializable {
	private int id;
	private Book book;
	private User User;
	private Date createTime;
	private Date endTime;
	private int count;
	private int flag;
	private String flagStr;
	private String createTimeStr;
	private String endTimeStr;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public String getCreateTimeStr() {
		if (createTime != null) {
			createTimeStr = DateUtils.utilDateToStr(createTime, "yyyy-MM-dd HH:mm:ss");
		}
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

	public String getEndTimeStr() {
		if (endTime != null) {
			endTimeStr = DateUtils.utilDateToStr(endTime, "yyyy-MM-dd");
		}
		return endTimeStr;
	}

	public void setEndTimeStr(String endTimeStr) {
		this.endTimeStr = endTimeStr;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public User getUser() {
		return User;
	}

	public void setUser(User user) {
		User = user;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getFlagStr() {
		return flag == 1 ? "已还" : "未还";
	}

	public void setFlagStr(String flagStr) {
		this.flagStr = flagStr;
	}

}
