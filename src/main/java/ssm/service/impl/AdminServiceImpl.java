package ssm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ssm.mapper.IAdminMapper;
import ssm.mapper.IUserMapper;
import ssm.pojo.Admin;
import ssm.pojo.PageInfo;
import ssm.pojo.User;
import ssm.service.IAdminService;
import ssm.service.IUserService;

/**
 * @Author: lzp
 * @Date: 2020/9/30 9:58
 * @Description:
 */
@Service
public class AdminServiceImpl implements IAdminService {

    @Autowired
    IAdminMapper adminMapper;

    @Override
    public Admin findById(int id) {
        return adminMapper.findById(id);
    }

    @Override
    public Admin login(Admin admin) {
        return adminMapper.find(admin);
    }

    @Override
    public boolean changePwd(Admin admin) {
        return adminMapper.changePassword(admin)>0;
    }
}
