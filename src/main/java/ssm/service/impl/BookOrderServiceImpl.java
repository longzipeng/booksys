package ssm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ssm.mapper.IBookOrderMapper;
import ssm.pojo.BookOrder;
import ssm.pojo.PageInfo;
import ssm.service.IBookOrderService;

import java.util.Date;
import java.util.List;

/**
 * @Author: lzp
 * @Date: 2020/10/5 20:11
 * @Description: 书籍订单实现类
 */
@Service
public class BookOrderServiceImpl implements IBookOrderService {

    @Autowired
    IBookOrderMapper bookOrderMapper;

    @Override
    public boolean add(BookOrder bookOrder) {
        bookOrder.setCreateTime(new Date());
        return bookOrderMapper.add(bookOrder)>0;
    }

    @Override
    public boolean update(BookOrder bookOrder) {
        return bookOrderMapper.update(bookOrder)>0;
    }

    @Override
    public List<BookOrder> findAllJson() {
        return bookOrderMapper.findAllJson();
    }

    @Override
    public void findAll(PageInfo<BookOrder> pageInfo) {
        pageInfo.setTotal(bookOrderMapper.findTotal(pageInfo));
        pageInfo.setRows(bookOrderMapper.findAll(pageInfo));
    }

    @Override
    public BookOrder findById(int id) {
        return bookOrderMapper.findById(id);
    }

    @Override
    public List<BookOrder> findByUserId(int id) {
        return bookOrderMapper.findByUserId(id);
    }

    @Override
    public boolean del(int id) {
        return bookOrderMapper.del(id)>0;
    }
}
