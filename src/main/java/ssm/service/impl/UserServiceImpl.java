package ssm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ssm.mapper.IUserMapper;
import ssm.pojo.PageInfo;
import ssm.pojo.User;
import ssm.service.IUserService;

/**
 * @Author: lzp
 * @Date: 2020/9/30 9:58
 * @Description:
 */
@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    IUserMapper userMapper;

    @Override
    public boolean register(User user) {
        return userMapper.add(user)>0;
    }

    @Override
    public User login(String username, String password) {
        return userMapper.find(username,password);
    }

    @Override
    public boolean update(User user) {
        return userMapper.update(user)>0;
    }

    @Override
    public boolean changePwd(User user) {
        return userMapper.changePassword(user)>0;
    }

    @Override
    public User findById(int id) {
        return userMapper.findById(id);
    }

    @Override
    public boolean add(User user) {
        return userMapper.add(user)>0;
    }

    @Override
    public boolean del(int id) {
        return userMapper.del(id)>0;
    }

    @Override
    public void findAll(PageInfo<User> pageInfo) {
        pageInfo.setRows(userMapper.findAll(pageInfo));
        pageInfo.setTotal(userMapper.findTotal(pageInfo));
    }

    @Override
    public User findByUsername(String username) {
        return userMapper.findByUsername(username);
    }
}
