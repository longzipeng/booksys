package ssm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ssm.mapper.ITypeMapper;
import ssm.pojo.PageInfo;
import ssm.pojo.Type;
import ssm.service.IBookService;
import ssm.service.ITypeService;

import java.util.List;

/**
 * @Author: lzp
 * @Date: 2020/10/5 10:14
 * @Description: 分类业务层实现类
 */
@Service
public class TypeServiceImpl implements ITypeService {

    @Autowired
    ITypeMapper typeMapper;

    @Override
    public Type findById(int typeId) {
        return typeMapper.findById(typeId);
    }

    @Override
    public List<Type> findAllJson() {
        return typeMapper.findAllJson();
    }

    @Override
    public void findAll(PageInfo<Type> pageInfo) {
        pageInfo.setTotal(typeMapper.findTotal(pageInfo));
        pageInfo.setRows(typeMapper.findAll(pageInfo));
    }

    @Override
    public boolean add(Type type) {
        return typeMapper.add(type)>0;
    }

    @Override
    public boolean del(int id) {
        return typeMapper.del(id)>0;
    }

    @Override
    public boolean update(Type type) {
        return typeMapper.update(type)>0;
    }
}
