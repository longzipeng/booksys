package ssm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ssm.mapper.IBookMapper;
import ssm.pojo.Book;
import ssm.pojo.PageInfo;
import ssm.service.IBookService;

/**
 * @Author: lzp
 * @Date: 2020/10/5 10:14
 * @Description: 书籍业务层实现类
 */
@Service
public class BookServiceImpl implements IBookService {

    @Autowired
    IBookMapper bookMapper;

    @Override
    public void findAll(PageInfo<Book> pageInfo) {
        pageInfo.setRows(bookMapper.findAll(pageInfo));
        pageInfo.setTotal(bookMapper.findTotal(pageInfo));
    }

    @Override
    public boolean add(Book book) {
        return bookMapper.add(book)>0;
    }

    @Override
    public Book findById(int id) {
        return bookMapper.findById(id);
    }

    @Override
    public boolean update(Book book) {
        return bookMapper.update(book)>0;
    }

    @Override
    public boolean del(int id) {
        return bookMapper.del(id)>0;
    }

    @Override
    public void findByTypeId(PageInfo<Book> pageInfo) {
        pageInfo.setRows(bookMapper.findByTypeId(pageInfo));
        pageInfo.setTotal(bookMapper.findTotal(pageInfo));
    }
}
