package ssm.service;

import ssm.pojo.Admin;
import ssm.pojo.PageInfo;
import ssm.pojo.User;

/**
 * @author Bacon
 *2020年9月3日
 */
public interface IAdminService {

	/**
	 * 通过id查找单个
	 * @param id
	 * @return
	 */
	public Admin findById(int id);

	/**
	 * 登录
	 * @param admin
	 * @return
	 */
	public Admin login(Admin admin);

	/**
	 * 修改密码
	 * @param admin
	 * @return
	 */
	public boolean changePwd(Admin admin);

}
