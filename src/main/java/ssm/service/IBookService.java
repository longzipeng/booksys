package ssm.service;

import ssm.pojo.Book;
import ssm.pojo.PageInfo;

/**
 * @Author: lzp
 * @Date: 2020/10/5 10:14
 * @Description:
 */
public interface IBookService {
    void findAll(PageInfo<Book> pageInfo);

    boolean add(Book book);

    Book findById(int id);

    boolean update(Book book);

    boolean del(int id);

    void findByTypeId(PageInfo<Book> pageInfo);
}
