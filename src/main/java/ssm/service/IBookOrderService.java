package ssm.service;

import ssm.pojo.BookOrder;
import ssm.pojo.PageInfo;

import java.util.List;

/**
 * @Author: lzp
 * @Date: 2020/10/5 10:14
 * @Description:
 */
public interface IBookOrderService {

    boolean add(BookOrder bookOrder);

    boolean update(BookOrder bookOrder);

    List<BookOrder> findAllJson();

    void findAll(PageInfo<BookOrder> pageInfo);

    BookOrder findById(int id);

    List<BookOrder> findByUserId(int id);

    boolean del(int id);
}
