package ssm.service;

import ssm.pojo.PageInfo;
import ssm.pojo.Type;

import java.util.List;

/**
 * @Author: lzp
 * @Date: 2020/10/5 10:14
 * @Description:
 */
public interface ITypeService {

    /**
     * 通过id查找分类
     * @param typeId
     * @return
     */
    Type findById(int typeId);

    /**
     * 以Json格式返回所有分类
     * @return
     */
    List<Type> findAllJson();

    /**
     * 带分页的查询所有分类
     * @param pageInfo
     */
    void findAll(PageInfo<Type> pageInfo);

    /**
     * 添加分类
     * @param type
     * @return
     */
    boolean add(Type type);

    /**
     * 删除单个分类数据
     * @param id
     * @return
     */
    boolean del(int id);

    /**
     * 修改分类
     * @param type
     * @return
     */
    boolean update(Type type);
}
