package ssm.service;

import ssm.pojo.PageInfo;
import ssm.pojo.User;

/**
 * @author Bacon
 *2020年9月3日
 */
public interface IUserService {

	public boolean register(User user);

	public User login(String username, String password);

	public boolean update(User user);

	public boolean changePwd(User user);

	public User findById(int id);
	
	public boolean add(User user);

	public boolean del(int id);

	public void findAll(PageInfo<User> pageInfo);

	public User findByUsername(String username);


}
