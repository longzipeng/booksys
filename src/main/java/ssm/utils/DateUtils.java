package ssm.utils;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * String --> util.Date
 * util.Date --> String
 * util.Date --> sql.Date
 * sql.Date --> util.Date   自动转
 * @Date 2020年8月11日 上午11:32:41
 */
public final class DateUtils {
	
	private static DateFormat df = null;
	
	/**
	 * 字符串 转 util.Date
	 * @param str
	 * @return
	 */
	public static Date strToUtilDate(String str,String par){
		df = new SimpleDateFormat(par);
		try {
			return df.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 *  util.Date 转  字符串
	 * @param  date,par
	 * @return
	 */
	public static String utilDateToStr(Date date,String par){
		df = new SimpleDateFormat(par);
		return df.format(date);
	}
	/**
	 * util.Date 转  Sql.Date
	 * @param date
	 * @return
	 */
	public static java.sql.Date utilToSql(Date date,String par){
		df = new SimpleDateFormat(par);
		return new java.sql.Date(date.getTime());
	}

}
