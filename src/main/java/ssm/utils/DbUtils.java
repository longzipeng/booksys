package ssm.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

/**
 * @Date 2020年8月12日 下午4:11:36
 */
public final class DbUtils {
	private static DruidDataSource ds = null;
	private static Connection conn = null;
	
	private DbUtils(){}
	
	static{
		try {
			Properties prop = new Properties();
			InputStream is = DbUtils.class.getClassLoader().getResourceAsStream("db.properties");
			prop.load(is);
			is.close();
			ds = (DruidDataSource)DruidDataSourceFactory.createDataSource(prop);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection(){
		try {
			conn = ds.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return conn;
	}
	
	public static DataSource getDataSource(){
		return ds;
	}
}
