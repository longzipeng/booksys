package ssm.mapper;

import org.apache.ibatis.annotations.*;
import ssm.pojo.Book;
import ssm.pojo.PageInfo;
import ssm.pojo.Type;

import java.util.List;

/**
 * @Author: lzp
 * @Date: 2020/10/5 10:12
 * @Description: book dao层
 */
public interface IBookMapper {

    List<Book> findAll(PageInfo<Book> pageInfo);

    List<Book> findByTypeId(PageInfo<Book> pageInfo);

    @Delete("delete from book where id = #{id}")
    int del(int id);

    int update(Book book);

    Book findById(int id);

    @Insert("INSERT INTO book VALUES (null,#{name},#{author},#{publish},#{publishTime}," +
            "#{introduction},#{price},#{img},#{type.id},#{count},#{createTime})")
    int add(Book book);

    int findTotal(PageInfo<Book> pageInfo);
}
