package ssm.mapper;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import ssm.pojo.Admin;

/**
 * @Author: lzp
 * @Date: 2020/9/30 9:45
 * @Description:
 */

public interface IAdminMapper {

    @Select("select * from admin where id = #{id}")
    public Admin findById(int id);

    @Select("select * from admin where username=#{username} and password=#{password}")
    public Admin find(Admin admin);

    @Update("update admin set password=#{password} where id=#{id}")
    public int changePassword(Admin admin);
}
