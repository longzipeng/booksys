package ssm.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import ssm.pojo.PageInfo;
import ssm.pojo.Type;

import java.util.List;

/**
 * @Author: lzp
 * @Date: 2020/10/5 10:12
 * @Description: book dao层
 */
public interface ITypeMapper {

    @Select("select * from type where id=#{id}")
    Type findById(int id);

    @Select("select * from type")
    List<Type> findAllJson();

    List<Type> findAll(PageInfo<Type> pageInfo);

    int findTotal(PageInfo<Type> pageInfo);

    @Insert("insert into type values(default,#{name},default)")
    int add(Type type);

    @Update("update type set name = #{name} where id =#{id} ")
    int update(Type type);

    @Delete("DELETE FROM TYPE WHERE id = #{id}")
    int del(int id);
}
