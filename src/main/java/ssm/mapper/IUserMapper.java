package ssm.mapper;

import org.apache.ibatis.annotations.*;
import ssm.pojo.PageInfo;
import ssm.pojo.User;

import java.util.List;

/**
 * @Author: lzp
 * @Date: 2020/9/30 9:45
 * @Description:
 */

public interface IUserMapper {

    @Insert("insert into user values(null,#{username},#{password},#{name},#{money},#{email},null)")
    public int add(User user);

    @Select("select * from user where username=#{username} and password=#{password}")
    public User find(@Param("username") String username, @Param("password") String password);

    @Update("update user set name=#{name},password=#{password},email=#{email},money=#{money} where id=#{id}")
    public int update(User user);

    @Update("UPDATE user SET password=#{password} where id=#{id}")
    public int changePassword(User user);

    @Select("select * from user where id = #{id}")
    public User findById(int id);

    @Delete("delete from user where id= #{id}")
    public int del(int id);

    public List<User> findAll(PageInfo<User> pageInfo);

    public int findTotal(PageInfo<User> pageInfo);

    @Select("select * from user where username = #{username}")
    public User findByUsername(String username);
}
