package ssm.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import ssm.pojo.BookOrder;
import ssm.pojo.PageInfo;

import java.util.List;

/**
 * @Author: lzp
 * @Date: 2020/9/30 9:45
 * @Description:
 */

public interface IBookOrderMapper {


    @Insert("insert into bookorder values(null,#{book.id},#{user.id},null,#{endTime},#{count},0)")
    int add(BookOrder bookOrder);

    List<BookOrder> findAllJson();

    List<BookOrder> findAll(PageInfo<BookOrder> pageInfo);

    int findTotal(PageInfo<BookOrder> pageInfo);

    BookOrder findById(int id);

    List<BookOrder> findByUserId(int id);

    @Delete("delete from bookorder where id = #{id}")
    int del(int id);

    @Update("update bookorder set bookId = #{book.id},userId = #{user.id},endTime = #{endTime},count=#{count},flag=#{flag} where id = #{id}")
    int update(BookOrder bookOrder);
}
