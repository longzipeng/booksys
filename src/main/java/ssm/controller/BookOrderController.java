package ssm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ssm.pojo.Book;
import ssm.pojo.BookOrder;
import ssm.pojo.PageInfo;
import ssm.pojo.Result;
import ssm.service.IBookOrderService;
import ssm.service.IBookService;
import ssm.service.IUserService;
import ssm.utils.DateUtils;

import java.util.List;


/**
 * @Author: lzp
 * @Date: 2020/10/5 20:08
 * @Description: 书籍订单控制器
 */
@Controller
@RequestMapping("/order")
public class BookOrderController {
    @Autowired
    IBookOrderService bookOrderService;
    @Autowired
    IBookService bookService;
    @Autowired
    IUserService userService;

    /**
     * 增加订单
     * @param bookId
     * @param userId
     * @param count
     * @param endTime
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public Result add(int bookId,int userId,int count,String endTime) {
        Result resule = new Result();
        try {
            BookOrder bookOrder = new BookOrder();
            bookOrder.setBook(bookService.findById(bookId));
            bookOrder.setUser(userService.findById(userId));
            bookOrder.setEndTime(DateUtils.strToUtilDate(endTime, "yyyy-MM-dd"));
            bookOrder.setCount(count);
            boolean flag = bookOrderService.add(bookOrder);
            resule.setFlag(flag);
            if (flag) {
                resule.setMsg("添加成功！！");
            } else {
                resule.setMsg("添加失败！！请检查书籍或用户是否存在！！");
            }
        } catch (Exception e) {
            resule.setFlag(false);
            resule.setMsg("添加失败！！请检查书籍或用户是否存在！！");
        }
        return  resule;
    }

    /**
     * 增加修改订单
     * @param bookId
     * @param userId
     * @param count
     * @param endTime
     * @param bookOrder
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public Result update(int bookId,int userId,int count,String endTime,BookOrder bookOrder) {
        Result result = new Result();
        try {
            bookOrder.setBook(bookService.findById(bookId));
            bookOrder.setUser(userService.findById(userId));
            bookOrder.setEndTime(DateUtils.strToUtilDate(endTime, "yyyy-MM-dd"));
            bookOrder.setCount(count);
            bookOrder.setId(userId);
            boolean flag = bookOrderService.update(bookOrder);
            result.setFlag(flag);
            if (flag) {
                result.setMsg("修改成功！！");
            } else {
                result.setMsg("修改失败！！请检测书籍或用户是否存在！！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setFlag(false);
            result.setMsg("修改失败！！请检测书籍或用户是否存在！！");
        }
        return result;
    }

    /**
     * 查找所有订单并JSON返回
     * @return
     */
    @RequestMapping("/findAllJson")
    @ResponseBody
    public List<BookOrder> findAllJson() {
        return bookOrderService.findAllJson();
    }

    /**
     * 查找所有带分页
     * @param pageInfo
     * @return
     */
    @RequestMapping("/findAll")
    @ResponseBody
    public PageInfo<BookOrder> findAll(PageInfo<BookOrder> pageInfo) {
        // 通过getParameter设置参数，因为已经做了get请求的乱码处理
        pageInfo.setKeyWord("%" + pageInfo.getKeyWord() + "%");
        bookOrderService.findAll(pageInfo);
        return pageInfo;
    }

    /**
     * 通过ID查询订单
     * @param id
     * @return
     */
    @RequestMapping("/findById")
    @ResponseBody
    public BookOrder findById(int id) {
        return bookOrderService.findById(id);
    }

    /**
     * 通过客户ID查询订单
     * @param id
     * @return
     */
    @RequestMapping("/findByUserId")
    @ResponseBody
    public List<BookOrder> findByUserId(int id) {
        return bookOrderService.findByUserId(id);
    }

    /**
     * 单个删除
     * @param id
     * @return
     */
    @RequestMapping("/del")
    @ResponseBody
    public Result del(int id) {
        boolean flag = bookOrderService.del(id);
        Result result = new Result();
        result.setFlag(flag);
        if (flag) {
            result.setMsg("订单删除成功！！");
        } else {
            result.setMsg("订单删除失败！！");
        }
        return result;
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result delete(int[] ids) {
        boolean flag = true;
        for (int id : ids) {
            if (!bookOrderService.del(id)) {
                flag = false;
            }
        }
        Result result = new Result();
        result.setFlag(flag);
        if (flag) {
            result.setMsg("订单删除成功！！");
        } else {
            result.setMsg("订单删除失败！！");
        }
        return result;
    }

}
