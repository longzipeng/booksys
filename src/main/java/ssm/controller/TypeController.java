package ssm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ssm.pojo.PageInfo;
import ssm.pojo.Result;
import ssm.pojo.Type;
import ssm.service.ITypeService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author: lzp
 * @Date: 2020/10/5 10:16
 * @Description:
 */
@Controller
@RequestMapping("/type")
public class TypeController {

    @Autowired
    ITypeService typeService;

    /**
     * 查找所有的Type并返回JSON数据
     * @return
     */
    @RequestMapping("/findAllJson")
    @ResponseBody
    public List<Type> findAllJson(){
        List<Type> list = typeService.findAllJson();
        return list;
    }

    /**
     * 查找所有分类带分页
     * @param pageInfo
     * @return
     */
    @RequestMapping("/findAll")
    @ResponseBody
    public PageInfo<Type> findAll(PageInfo<Type> pageInfo) {
        pageInfo.setKeyWord("%"+pageInfo.getKeyWord()+"%");
        System.out.println(pageInfo.getKeyWord());
        typeService.findAll(pageInfo);
        return pageInfo;
    }


    /**
     * 增加数据
     * @param type
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public Result add(Type type) {
        boolean flag = typeService.add(type);
        Result result = new Result();
        result.setFlag(flag);
        if (flag) {
            result.setMsg("添加成功");
        }else {
            result.setMsg("添加失败");
        }
        return result;
    }

    /**
     * 通过id查找分类
     * @param id
     * @return
     */
    @RequestMapping("/findById")
    @ResponseBody
    public Type findById(int id) {
        return typeService.findById(id);
    }

    /**
     * 修改分类信息
     * @param type
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public Result update(Type type) {
        boolean flag = typeService.update(type);
        Result result = new Result();
        result.setFlag(flag);
        if (flag) {
            result.setMsg("修改成功");
        }else {
            result.setMsg("修改失败");
        }
        return result;
    }

    /**
     * 删除单个
     * @param id
     * @return
     */
    @RequestMapping("/del")
    @ResponseBody
    public Result del(int id) {
        boolean flag = typeService.del(id);
        Result result = new Result();
        result.setFlag(flag);
        if(flag) {
            result.setMsg("图书删除成功！！");
        }else {
            result.setMsg("图书删除失败！！");
        }
        return result;
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result delete(int[] ids) {
        boolean flag = true;
        for (int id : ids) {
            if(!typeService.del(id)) {
                flag = false;
            }
        }
        Result result = new Result();
        result.setFlag(flag);
        if(flag) {
            result.setMsg("删除成功！！");
        }else {
            result.setMsg("删除失败！！");
        }
        return result;
    }
}
