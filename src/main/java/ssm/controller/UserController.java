package ssm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ssm.pojo.PageInfo;
import ssm.pojo.Result;
import ssm.pojo.User;
import ssm.service.IUserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @Author: lzp
 * @Date: 2020/9/30 9:55
 * @Description:
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    IUserService userService;
    @Autowired
    HttpSession session;

    /**
     * 查询所有
     *
     * @param pageInfo
     * @return
     */
    @RequestMapping("/findAll")
    @ResponseBody
    public PageInfo<User> findAll(PageInfo<User> pageInfo) {
        pageInfo.setKeyWord("%"+pageInfo.getKeyWord()+"%");
        userService.findAll(pageInfo);
        return pageInfo;
    }


    /**
     * 通过用户名查找用户
     *
     * @param username
     * @return
     */
    @RequestMapping("/findByUsername")
    @ResponseBody
    public User findByUsername(String username) {
        User user = userService.findByUsername(username);
        return user;
    }


    /**
     * 注册用户
     *
     * @param user
     * @param checkCode
     * @return
     */
    @RequestMapping("/register")
    @ResponseBody
    public Result register(User user, String checkCode) {
        //封装参数
        String CHECKNUM = (String) session.getAttribute("CHECKNUM");
        Result result = new Result();
        if (!CHECKNUM.equals(checkCode)) {
            result.setFlag(false);
            result.setMsg("验证码错误！！");
            return result;
        }
        user.setMoney(0);
        //处理结果集
        boolean flag = userService.register(user);
        result.setFlag(flag);
        if (flag) {
            result.setMsg("注册成功，请登录");
        } else {
            result.setMsg("注册失败，可能是用户名重复！！");
        }
        return result;
    }

    /**
     * 登录
     *
     * @param loginUser
     * @param checkCode
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/login")
    @ResponseBody
    public Result login(User loginUser, String checkCode, HttpServletResponse response) throws ServletException, IOException {
        String CHECKNUM = (String) session.getAttribute("CHECKNUM");
        Result result = new Result();
        if (!CHECKNUM.equals(checkCode)) {
            result.setFlag(false);
            result.setMsg("验证码错误！！");
            return result;
        }
        User user = userService.login(loginUser.getUsername(), loginUser.getPassword());
        if (user != null) {
            //成功登陆
            if (user.getPassword().equals(loginUser.getPassword())) {
                Cookie cookie = new Cookie("JSESSIONID", session.getId());
                cookie.setMaxAge(24 * 60 * 60);
                response.addCookie(cookie);
                session.setAttribute("user", user);
                result.setFlag(true);
                result.setMsg("亲爱的" + user.getName() + "欢迎回来！！");
                return result;
            }
        }
        result.setFlag(false);
        result.setMsg("账号或密码错误！！");
        return result;
    }

    /**
     * 修改密码
     *
     * @param password
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/changePwd")
    @ResponseBody
    public Result changePwd(String password) throws ServletException, IOException {
        Result result = new Result();
        if (session != null) {
            User user = (User) session.getAttribute("user");
            if (user != null) {
                user.setPassword(password);
                boolean flag = userService.changePwd(user);
                result.setFlag(flag);
                if (flag) {
                    result.setMsg("密码修改成功！！");
                } else {
                    result.setMsg("密码修改失败！！");
                }
                return result;
            }
        }
        result.setFlag(false);
        result.setMsg("密码修改失败！！");
        return result;
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/findById")
    @ResponseBody
    public User findById(String id) throws ServletException, IOException {
        User user = userService.findById(Integer.parseInt(id));
        return user;
    }

    /**
     * 修改个人信息
     *
     * @param user
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/update")
    @ResponseBody
    public Result update(User user) throws ServletException, IOException {
        Result result = new Result();
        boolean flag = userService.update(user);
        result.setFlag(flag);
        if (flag) {
            result.setMsg("修改成功！！");
        } else {
            result.setMsg("修改失败！！");
        }
        return result;
    }

    /**
     * 注销
     *
     * @param request
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/logout")
    public String logout(HttpServletRequest request) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.removeAttribute("user");
        }
        return "redirect:/app/login";
    }

    /**
     * 添加用户
     *
     * @param user
     * @return result
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/add")
    @ResponseBody
    public Result add(User user) throws ServletException, IOException {
        Result result = new Result();
        boolean flag = userService.add(user);
        result.setFlag(flag);
        if (flag) {
            result.setMsg("添加成功！！");
        } else {
            result.setMsg("添加失败！！");
        }
        return result;
    }

    /**
     * 删除单个用户
     *
     * @param id
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/del")
    @ResponseBody
    public Result del(int id) throws ServletException, IOException {
        boolean flag = userService.del(id);
        Result result = new Result();
        result.setFlag(flag);
        if (flag) {
            result.setMsg("删除成功！！");
        } else {
            result.setMsg("删除失败！！");
        }
        return result;
    }

    /**
     * 批量删除
     *
     * @param ids
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/delete")
    @ResponseBody
    public Result delete(int[] ids) throws ServletException, IOException {
        boolean flag = true;
        for (int id : ids) {
            if (!userService.del(id)) {
                flag = false;
            }
        }
        Result result = new Result();
        result.setFlag(flag);
        if (flag) {
            result.setMsg("删除成功！！");
        } else {
            result.setMsg("删除失败！！");
        }
        return result;
    }

}
