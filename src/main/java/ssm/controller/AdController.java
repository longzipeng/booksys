package ssm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Author: lzp
 * @Date: 2020/9/30 11:03
 * @Description:
 */
@Controller()
@RequestMapping("/ad")
public class AdController {

    /**
     * 跳转登录
     * @return
     */
    @RequestMapping("login")
    public String login() {
        return "comment/login";
    }

    /**
     * 跳转后端主页
     * @return
     */
    @RequestMapping("index")
    public String index() {
        return "admin/index";
    }

    /**
     * 跳转后端frame主页
     * @return
     */
    @RequestMapping("main")
    public String main() {
        return "admin/main";
    }

    /**
     * 跳转书籍管理
     * @return
     */
    @RequestMapping("books")
    public String books() {
        return "admin/books";
    }

    /**
     * 跳转分类管理
     * @return
     */
    @RequestMapping("types")
    public String types() {
        return "admin/types";
    }

    /**
     * 跳转用户管理
     * @return
     */
    @RequestMapping("users")
    public String users() {
        return "admin/users";
    }

    /**
     * 跳转用户管理
     * @return
     */
    @RequestMapping("orders")
    public String orders() {
        return "admin/orders";
    }
}
