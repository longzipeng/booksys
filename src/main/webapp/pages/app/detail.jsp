<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>详情--${book.name}</title>
    <%@include file="title.jsp" %>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/plugins/Bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/detail.css">
    <style type="text/css">
        body {
            background: url(${pageContext.request.contextPath}/static/img/a13.jpg) no-repeat center center fixed;
            background-size: cover;
            font-size: 16px;
            color: lightgray !important;
        }

        .container {
            background-color: rgba(0, 0, 0, .4);
            border-radius: 5px;
        }

        .container .jx h3 {
            color: white;
        }

        .container .myTag a div {
            background-color: rgba(0, 0, 0, 0.7);
            transition: all 0.5s;
        }

        .container .myTag a div:hover {
            background-color: rgba(0, 0, 0, .9);
            color: white;
        }
    </style>
    <script type="text/javascript">
        function buy(id) {
            if ('${user}' == "" || '${user}' == null || '${user}' == undefined) {
                myLayer.errorAlert("登录后才能借书！！");
                setTimeout(function () {
                    location.href = "${pageContext.request.contextPath}/app/login";
                }, 2000);
            } else {
                $('#count').val(0);
                $('#spend').text("0");
                $('#endTime').val(undefined);
                $('#orderModal').modal('show');
            }
        }
    </script>
</head>
<body>
<header class="container-fluid" style="width: 100%;margin: 0px;">
    <%@include file="header.jsp" %>
</header>
<div class="container">
    <div class="row jx">
        <h3><span class="glyphicon glyphicon-tags"></span> 图书详情介绍</h3>
    </div>
    <div class="row padding-top">
        <div class="col-md-3">
            <img src="${pageContext.request.contextPath}/upload/face/${book.img}" class="img-responsive"/>
        </div>
        <div class="col-md-9">
            <div class="row">
                <!-- <div class="thumbnail"> -->
                <div class="col-sm-0"></div>
                <div class="col-sm-9">
                    <h4>名 称 : ${book.name}</h4>
                </div>
                <div class="col-sm-3">
                </div>
                <!-- </div> -->
            </div>
            <div class="row">
                <div class="col-sm-0"></div>
                <div class="col-sm-9">
                    <h4>作 者 : ${book.author}</h4>
                </div>
                <div class="col-sm-3">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-0"></div>
                <div class="col-sm-9">
                    <h4>出 版 社 : ${book.publish}</h4>
                </div>
                <div class="col-sm-3">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-0"></div>
                <div class="col-sm-9">
                    <h4>价 格 : <font color="hotpink">${book.price}</font>元</h4>
                </div>
                <div class="col-sm-3">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-0"></div>
                <div class="col-sm-9">
                    <h4>库 存 : <font color="hotpink">${book.count}</font> 件</h4>
                </div>
                <div class="col-sm-3">
                </div>
            </div>
            <div class="row jx">
                <!-- <h3><span class="glyphicon glyphicon-tags"></span>     图书详情介绍</h3> -->
            </div>
            <div class="row">
                <div class="col-sm-0"></div>
                <div class="col-sm-9">
                    <h4>反 馈 意 见 : </h4>
                </div>
                <div class="col-sm-3">
                </div>
            </div>
            <div class="row myTag">
                <a href="#">
                    <div class="col-md-1.5 col-sm-1"></div>
                </a>
                <a href="#">
                    <div class="col-md-1.5 col-sm-2 inner">视觉干扰</div>
                </a>
                <a href="#">
                    <div class="col-md-1.5 col-sm-1"></div>
                </a>
                <a href="#">
                    <div class="col-md-1.5 col-sm-2 inner">内容不宜</div>
                </a>
                <a href="#">
                    <div class="col-md-1.5 col-sm-1"></div>
                </a>
                <a href="#">
                    <div class="col-md-1.5 col-sm-2 inner">不感兴趣</div>
                </a>
                <a href="#">
                    <div class="col-md-1.5 col-sm-1"></div>
                </a>
            </div>
            <div class="row">
                <div class="col-sm-0"></div>
                <div class="col-sm-9">
                    <h4>下 单 请 点 : </h4>
                </div>
                <div class="col-sm-3">
                </div>
            </div>
            <div class="row myTag">
                <!-- <a href="#"><div class="col-md-1.5 col-sm-1"></div></a> -->
                <a href="#">
                    <div class="col-md-1.5 col-sm-1"></div>
                </a>
                <a href="#">
                    <div class="col-md-1.5 col-sm-1"></div>
                </a>
                <a href="#">
                    <div class="col-md-1.5 col-sm-2 inner"><span class="glyphicon glyphicon-shopping-cart"></span> 加入购物车
                    </div>
                </a>
                <a href="#">
                    <div class="col-md-1.5 col-sm-1"></div>
                </a>
                <a href="javascript:buy(${book.id})">
                    <div class="col-md-1.5 col-sm-2 inner"><span class="glyphicon glyphicon-thumbs-up"></span> 立即借阅
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row jx">
        <h3><span class="glyphicon glyphicon-tags"></span> 图书内容简介</h3>
    </div>
    <div class="row">
        <div class="box">
            <p>&copy;作${book.introduction}</p>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>
<!-- 编辑分类model -->
<div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     style="border-radius: 5px;">
    <div class="modal-dialog" role="document" class="background-color:rgba(0,0,0,.7);">
        <div class="modal-content" style="background-color: ghostwhite;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="updateLabel" style="color: orange;">借书订单</h4>
            </div>
            <div class="modal-body">
                <form id="orderForm" action="" method="POST">
                    <input type="hidden" name="bookId" value="${book.id}"/>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="mdi mdi-rename-box"></i>名称</span>
                                <input type="text" disabled="disabled" class="form-control" value="${book.name}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="mdi mdi-rename-box"></i>价格</span>
                                <input type="text" disabled="disabled" class="form-control" value="${book.price}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="mdi mdi-rename-box"></i>库存</span>
                                <input type="text" disabled="disabled" class="form-control" value="${book.count}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon">数量</span>
                                <input id="count" type="text" name="count" class="form-control"/>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon">还书时间</span>
                                <input id="endTime" type="date" name="endTime" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <p style="color: #333;font-size: 18px;">总计<span id="spend"
                                                                style="color: red;font-weight: bolder;font-size: 20px;">0</span>元
                </p>
                <button type="button" id="modal_add_btn" class="btn btn-success">提交</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    $('#count').change(function () {
        $("#spend").text($(this).val() *${book.price});
    });
    //点击提交订单参数
    $('#modal_add_btn').click(function () {
        var count = $.trim($('#count').val());
        var endTime = $('#endTime').val();
        if (count == null || count == undefined || count.length == 0) {
            myLayer.errorMsg("数量必须填写！！");
            return false;
        }
        if (endTime == null || endTime == undefined || endTime.length == 0) {
            myLayer.errorMsg("还书日期必须设置！！");
            return false;
        }
        var spend = $("#spend").text();
        var money = ${user.money};
        var bookCount = ${book.count};
        if (bookCount < count) {
            myLayer.errorAlert("库存不足！！");
            return false;
        }
        if (spend > money) {
            myLayer.errorAlert("余额不足，请充值！！");
            return false;
        }
        $.post("${pageContext.request.contextPath}/app/addOrder", $('#orderForm').serialize() + "&userId=${user.id}", function (data) {
            if (data.flag) {
                myLayer.successMsg(data.msg);
                $('#orderModal').modal('hide');
                setTimeout(function () {
                    location.href = "${pageContext.request.contextPath}/app/findBookById?id="+${book.id}+
                    "&rand=" + Math.random();
                }, 1000)
            } else {
                myLayer.errorMsg(data.msg);
                return false;
            }
        });
    });
</script>
</html>
