<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>青青草在线书城系统-搜索页面</title>
    <%@include file="title.jsp" %>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/plugins/Bootstrap/css/bootstrap.min.css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/plugins/Bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/search.css">
</head>
<body>
<header class="container-fluid">
    <%@include file="header.jsp" %>
</header>
<div class="container">
    <div class="row jx" style="color: lightgray;">
        <span class="glyphicon glyphicon-tags" style="color: skyblue;"></span> 共找到结果<font
            color="hotpink" style="font-weight: bold;">${pageInfo.total }</font>条
    </div>


    <div class="row">
        <c:if test="${not empty pageInfo.rows }">
            <c:forEach items="${pageInfo.rows }" var="book" varStatus="s">
                <div class="col-md-2">
                    <div class="thumbnail">
                        <a href="${pageContext.request.contextPath }/app/findBookById?id=${book.id}"><img width="150px" height="200px"
                                src="${pageContext.request.contextPath}/upload/face/${book.img}"
                                class="img-responsive img-list"/></a>
                        <p>${book.name}</p>
                        <a href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a><font
                            color="red">￥${book.price }</font><font id="kc">库存：<font>${book.count }</font></font>
                    </div>
                </div>
                <c:if test="${s.count % 6 ==0  && pageInfo.total != 6}">
                    <div class="col-sm-12 jx">
                    </div>
                </c:if>
            </c:forEach>
        </c:if>
        <c:if test="${empty pageInfo.rows }">
            <div class="col-md-12">
                <h1 style="text-align: center;color: orange;">没有查找到相关数据....</h1>
            </div>
        </c:if>
    </div>
</div>
<footer class="container" style="height: 50px;margin-bottom: 100px;">
    <div class="row">
        <div class="box">
            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li><a href="#" aria-label="Previous"> <span
                            aria-hidden="true">&laquo;</span>
                    </a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#" aria-label="Next"> <span aria-hidden="true">&raquo;</span>
                    </a></li>
                </ul>
            </nav>
        </div>
    </div>
</footer>
<%@include file="footer.jsp" %>
</body>
</html>