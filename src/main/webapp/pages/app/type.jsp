<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>分类列表页--${type.name}</title>
		<%@include file="title.jsp" %>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/plugins/Bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/index.css">
	</head>
	<body>
		<header class="container-fluid">
			<%@include file="header.jsp" %>
		</header>
		<div class="container">
			<div class="row">
				<img src="${pageContext.request.contextPath}/static/img/a12.png" class="img-responsive" width="100%" />
			</div>
			<div class="container" id="main">
				<div class="row jx">
				</div>
				<div class="row">
					<div class="col-md-12">
						<h1 style="text-align: center;color: orange;">数据加载中....</h1>
					</div>
				</div>
			</div>
		</div>
		<!-- 底部 -->
		<%@include file="footer.jsp" %>
	</body>
	<script type="text/javascript">
		$.ajaxSettings.async = false;
		var load = myLayer.load();
		$.get("${pageContext.request.contextPath }/book/findByTypeId", {
			keyWord: ${type.id},
			pageSize: 9999,
			offset: 0
		}, function(books) {
			books = books.rows;
			var mainH = "";
			if(books == null || books==undefined ||books.length != 0){
				mainH+=`
					<div class="row jx">
					</div>
				`;
				mainH+='<div class="row">';
				for (let i = 1;i<=books.length;i++){
					let book = books[i-1];
					mainH+=`
						<div class="col-md-2">
							<div class="thumbnail">
								<a href="${pageContext.request.contextPath }/app/findBookById?id=`+book.id+`"><img width="150px" height="200px" src="${pageContext.request.contextPath}/upload/face/`+book.img+`" class="img-responsive" /></a>
								<p>`+book.name+`</p>
								<a href="#"><span class="glyphicon glyphicon-heart-empty"></span></a>
								<font color="red">￥`+book.price+`</font>
								<font id="kc">库存：<font>`+book.count+`</font>
								</font>
							</div>
						</div>
					`;
					if(i%6==0){
						mainH+='</div>';
						mainH+=`
							<div class="row jx">
							</div>
						`;
						if(i!=books.length){
							mainH+='<div class="row">';
						}
					}
				}
				if(books.length%6!=0){
					mainH+='</div>';
				}
			}else{
				mainH+=`
					<div class="row">
						<div class="col-md-12">
							<h1 style="text-align: center;color: orange;">没有这方面的书....</h1>
						</div>
					</div>
				`;
			}
			$('#main').html(mainH);
			layer.close(load);
		});
	</script>
</html>
