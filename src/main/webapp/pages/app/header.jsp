<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/plugins/Bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/lightyear/js/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/layer/layer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/lightyear/js/bootstrap.min.js"></script>
<style type="text/css">
	body {
		background: url(${pageContext.request.contextPath }/static/img/a13.jpg) no-repeat center center fixed;
		background-size: cover;
	}
</style>
<nav class="navbar navbar-inverse">
	<div class="container-fluid" style="width: 100%;margin: 0px;">
		<div class="navbar-header">
			<!--定义汉堡按钮-->
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
			 aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="${pageContext.request.contextPath }/app/index"><span class="glyphicon glyphicon-home" style="color: skyblue;"></span></a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav" id="bannerTop">
				<!-- ajax加载分类列表 -->
				分类列表加载中......
			</ul>
			<form id="search" class="navbar-form navbar-left" action="${pageContext.request.contextPath }/app/search" method="GET">
				<div class="form-group">
					<input type="text" class="form-control" name="keyWord" placeholder="书名,作者,描述" value="${pageInfo.keyWord}">
					<input type="hidden" name="pageSize" value="6">
				</div>
				<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
			</form>
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="${pageContext.request.contextPath}/app/register"><span class="glyphicon glyphicon-user"></span> 注册</a>
				</li>
				<li class="dropdown">
					<c:if test="${empty user}">
						<a href="${pageContext.request.contextPath}/app/login"><span class="glyphicon glyphicon-log-in"></span> 登录
						</a>
					</c:if>
					<c:if test="${not empty user}">
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
						 aria-expanded="false"><span class="glyphicon glyphicon-fire" style="color:red"></span> ${user.name } <span class="caret"></span></a>
					</c:if>
					<ul class="dropdown-menu">
						<li>
							<a href="#"><span class="glyphicon glyphicon-bell"></span> 个人信息</a>
						</li>
						<li>
							<a href="#"><span class="glyphicon glyphicon-pencil"></span> 修改密码</a>
						</li>
						<li>
							<a href="#"><span class="glyphicon glyphicon glyphicon-yen"></span>余额：${user.money}</a>
						</li>
						<li>
							<a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> 订单信息</a>
						</li>
						<li role="separator" class="divider"></li>
						<li>
							<a href="javascript:logOut()"><span class="glyphicon glyphicon-off"></span> 注销</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="#"><span class="glyphicon glyphicon-tags" style="color:#EEA236"></span> 我的订单</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
<script>
	//退出系统
	function logOut(){
		myLayer.confirm("确定退出系统？",function(){
			location.href = "${pageContext.request.contextPath }/user/logout";
		});
	}
	
	//遍历加载分类列表
	$.get("${pageContext.request.contextPath }/type/findAllJson",function(data){
		
		var $bannerTop = $('#bannerTop');
		var bannerTopH = "";
		var ulH = `
		<li class="dropdown">
			<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">其他
				<span class="caret"></span></a>
			<ul class="dropdown-menu">
		`;
		if(data.length>4){
			$(data).each(function(index){
				if(index<4){
					bannerTopH+='<li><a href="${pageContext.request.contextPath }/app/type?id='+this.id+'">'+this.name+'</a></li>';
				}else if(index == data.length - 1){
					ulH+='<li><a href="${pageContext.request.contextPath }/app/type?id='+this.id+'">'+this.name+'</a></li>'
				}else{
					ulH+='<li><a href="${pageContext.request.contextPath }/app/type?id='+this.id+'">'+this.name+'</a></li><li role="separator" class="divider"></li>'
				}
			})
			ulH+='</ul></li>'
			bannerTopH+=ulH;
		}else{
			$(data).each(function(index){
				if(index == data.length-1){
					bannerTopH+='<li><a href="${pageContext.request.contextPath }/app/type?id='+this.id+'">'+this.name+'</a></li>';
				}else{
					bannerTopH+='<li><a href="${pageContext.request.contextPath }/app/type?id='+this.id+'">'+this.name+'</a></li><li role="separator" class="divider"></li>';
				}
			});
		}
		$bannerTop.html(bannerTopH);
	});
</script>
