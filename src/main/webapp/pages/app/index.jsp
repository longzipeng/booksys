<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>青青草在线书城系统——主页</title>
		<%@include file="title.jsp" %>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/plugins/Bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/index.css">
		<style>
			.img-list {
				width: 150px;
				height: 200px;
			}
		</style>
	</head>
	<body>
		<header class="container-fluid">
			<%@include file="header.jsp" %>
			<div class="row">
				<div class="container">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
							<li data-target="#carousel-example-generic" data-slide-to="1"></li>
							<li data-target="#carousel-example-generic" data-slide-to="2"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img src="${pageContext.request.contextPath}/static/img/a8.jpg" alt="..." width="100%">
								<div class="carousel-caption">
								</div>
							</div>
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>
		</header>
		<div class="container" id="container">
			<!--主体部分1-->
			<div class="row jx">
				<span class="glyphicon glyphicon-tags"></span> 图书精选
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1 style="text-align: center;color: orange;">数据加载中....</h1>
				</div>
			</div>
		</div>
		<!--3、页脚部分-->
		<%@include file="footer.jsp" %>
		<script type="text/javascript">
			$.ajaxSettings.async = false;
			var load = myLayer.load();
			$.get("${pageContext.request.contextPath }/type/findAllJson",function(data){
				
				var booksH = "";
				$(data).each(function(index,el){
					//每个分类
					$.get("${pageContext.request.contextPath }/book/findByTypeId",{keyWord:el.id,pageSize:6,offset:0},function(books){
						books = books.rows;
						if(books.length!=0){
							let bookH = `
								<div class="row jx">
									<span class="glyphicon glyphicon-tags"></span> `+el.name+`
								</div>
							`;
							bookH+= '<div class="row">';
							for(let i=0;i<books.length;i++){
								let book = books[i];
								bookH+=`
								<div class="col-md-2">
									<div class="thumbnail">
										<a href="${pageContext.request.contextPath }/app/findBookById?id=`+book.id+`"><img width="150px" height="200px" src="${pageContext.request.contextPath}/upload/face/`+book.img+`" class="img-responsive" /></a>
										<p>`+book.name+`</p>
										<a href="#"><span class="glyphicon glyphicon-heart-empty"></span></a>
										<font color="red">￥`+book.price+`</font>
										<font id="kc">库存：<font>`+book.count+`</font>
										</font>
									</div>
								</div>
								`;
							}
							bookH+='</div>';
							booksH+=bookH;
						}
					})
				});
				$('#container').html(booksH);
				layer.close(load);
			})
			$.ajaxSettings.async = true;
		</script>
	</body>
</html>
