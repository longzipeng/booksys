<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<title>青青草在线书城系统——后台</title>
<link rel="icon"
	href="${pageContext.request.contextPath }/static/img/favicon.ico"
	type="image/ico">
<meta name="keywords" content="青青草在线书城系统">
<meta name="description" content="青青草在线书城系统">
<link
	href="${pageContext.request.contextPath }/static/plugins/lightyear/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath }/static/plugins/lightyear/css/materialdesignicons.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="${pageContext.request.contextPath }/static/plugins/lightyear/js/bootstrap-multitabs/multitabs.min.css">
<link
	href="${pageContext.request.contextPath }/static/plugins/lightyear/css/style.min.css"
	rel="stylesheet">
</head>

<body class="full-height-layout" data-logobg="color_8" data-headerbg="color_8" data-sidebarbg="color_8">
	<div class="lyear-layout-web">
		<div class="lyear-layout-container">
			<!--左侧导航-->
			<aside class="lyear-layout-sidebar">
				<!-- logo -->
				<div id="logo" class="sidebar-header">
					<a href="${pageContext.request.contextPath }/ad/index"><img
						src="${pageContext.request.contextPath }/static/img/logo-sidebar.png"
						title="图书管理系统" alt="图书管理系统" /></a>
				</div>
				<div class="lyear-layout-sidebar-scroll">

					<nav class="sidebar-main">
						<ul class="nav nav-drawer">
							<li class="nav-item active"><a class="multitabs"
								href="${pageContext.request.contextPath }/ad/main"><i
									class="mdi mdi-home"></i> <span>后台首页</span></a></li>
							<li class="nav-item nav-item-has-subnav"><a
								href="javascript:void(0)"><i
									class="mdi mdi-book-open-page-variant"></i> <span>书籍管理</span></a>
								<ul class="nav nav-subnav">
									<li><a class="multitabs"
										href="${pageContext.request.contextPath }/ad/books">书籍管理</a></li>
								</ul></li>
							<li class="nav-item nav-item-has-subnav"><a
								href="javascript:void(0)"><i
									class="mdi mdi-format-list-bulleted-type"></i> <span>分类管理</span></a>
								<ul class="nav nav-subnav">
									<li><a class="multitabs"
										href="${pageContext.request.contextPath }/ad/types">分类管理</a></li>
								</ul></li>
							<li class="nav-item nav-item-has-subnav"><a
								href="javascript:void(0)"><i
									class="mdi mdi-account-multiple"></i> <span>客户管理</span></a>
								<ul class="nav nav-subnav">
									<li><a class="multitabs"
										href="${pageContext.request.contextPath }/ad/users">客户管理</a></li>
								</ul></li>
							<li class="nav-item nav-item-has-subnav"><a
								href="javascript:void(0)"><i class="mdi mdi-file-outline"></i>
									<span>订单管理</span></a>
								<ul class="nav nav-subnav">
									<li><a class="multitabs"
										href="${pageContext.request.contextPath }/ad/orders">订单管理</a>
									</li>
								</ul></li>
							<li class="nav-item nav-item-has-subnav"><a
								href="javascript:void(0)"><i
									class="mdi mdi-dots-horizontal-circle"></i> <span>系统设置</span></a>
								<ul class="nav nav-subnav">
									<li><a class="multitabs" href="lyear_js_datepicker.html">系统设置</a>
									</li>
								</ul></li>

							<div class="sidebar-footer">
								<p class="copyright">
									Copyright &copy; 2020. <a target="_blank"
										href="http://lyear.itshubao.com"></a> All rights reserved.
								</p>
							</div>
						</ul>
					</nav>
				</div>

			</aside>
			<!--End 左侧导航-->
			<!--头部信息-->
			<header class="lyear-layout-header">

				<nav class="navbar navbar-default">
					<div class="topbar">

						<div class="topbar-left">
							<div class="lyear-aside-toggler">
								<span class="lyear-toggler-bar"></span> <span
									class="lyear-toggler-bar"></span> <span
									class="lyear-toggler-bar"></span>
							</div>
						</div>

						<ul class="topbar-right">
							<li class="dropdown dropdown-profile"><a
								href="javascript:void(0)" data-toggle="dropdown"> <img
									class="img-avatar img-avatar-48 m-r-10"
									src="${pageContext.request.contextPath }/static/upload/img/touxiang.jpg"
									alt="${admin.name}" /> <span>${admin.name} <span
										class="caret"></span></span>
							</a>
								<ul class="dropdown-menu dropdown-menu-right">
									<li><a class="multitabs"
										data-url="lyear_pages_profile.html" href="javascript:void(0)"><i
											class="mdi mdi-account"></i> 个人信息</a></li>
									<li><a href="javascript:void(0)" data-toggle="modal"
										data-target="#changePasswordModal"><i
											class="mdi mdi-lock-outline"></i> 修改密码</a></li>
									<li class="divider"></li>
									<li><a href="javascript:logOut()"><i
											class="mdi mdi-logout-variant"></i> 退出登录</a></li>
								</ul></li>
							<!--切换主题配色-->
							<li class="dropdown dropdown-skin"><span
								data-toggle="dropdown" class="icon-palette"><i
									class="mdi mdi-palette"></i></span>
								<ul class="dropdown-menu dropdown-menu-right"
									data-stopPropagation="true">
									<li class="drop-title">
										<p>LOGO</p>
									</li>
									<li class="drop-skin-li clearfix"><span class="inverse">
											<input type="radio" name="logo_bg" value="default"
											id="logo_bg_1" checked> <label for="logo_bg_1"></label>
									</span> <span> <input type="radio" name="logo_bg"
											value="color_2" id="logo_bg_2"> <label
											for="logo_bg_2"></label>
									</span> <span> <input type="radio" name="logo_bg"
											value="color_3" id="logo_bg_3"> <label
											for="logo_bg_3"></label>
									</span> <span> <input type="radio" name="logo_bg"
											value="color_4" id="logo_bg_4"> <label
											for="logo_bg_4"></label>
									</span> <span> <input type="radio" name="logo_bg"
											value="color_5" id="logo_bg_5"> <label
											for="logo_bg_5"></label>
									</span> <span> <input type="radio" name="logo_bg"
											value="color_6" id="logo_bg_6"> <label
											for="logo_bg_6"></label>
									</span> <span> <input type="radio" name="logo_bg"
											value="color_7" id="logo_bg_7"> <label
											for="logo_bg_7"></label>
									</span> <span> <input type="radio" name="logo_bg"
											value="color_8" id="logo_bg_8"> <label
											for="logo_bg_8"></label>
									</span></li>
									<li class="drop-title">
										<p>头部</p>
									</li>
									<li class="drop-skin-li clearfix"><span class="inverse">
											<input type="radio" name="header_bg" value="default"
											id="header_bg_1" checked> <label for="header_bg_1"></label>
									</span> <span> <input type="radio" name="header_bg"
											value="color_2" id="header_bg_2"> <label
											for="header_bg_2"></label>
									</span> <span> <input type="radio" name="header_bg"
											value="color_3" id="header_bg_3"> <label
											for="header_bg_3"></label>
									</span> <span> <input type="radio" name="header_bg"
											value="color_4" id="header_bg_4"> <label
											for="header_bg_4"></label>
									</span> <span> <input type="radio" name="header_bg"
											value="color_5" id="header_bg_5"> <label
											for="header_bg_5"></label>
									</span> <span> <input type="radio" name="header_bg"
											value="color_6" id="header_bg_6"> <label
											for="header_bg_6"></label>
									</span> <span> <input type="radio" name="header_bg"
											value="color_7" id="header_bg_7"> <label
											for="header_bg_7"></label>
									</span> <span> <input type="radio" name="header_bg"
											value="color_8" id="header_bg_8"> <label
											for="header_bg_8"></label>
									</span></li>
									<li class="drop-title">
										<p>侧边栏</p>
									</li>
									<li class="drop-skin-li clearfix"><span class="inverse">
											<input type="radio" name="sidebar_bg" value="default"
											id="sidebar_bg_1" checked> <label for="sidebar_bg_1"></label>
									</span> <span> <input type="radio" name="sidebar_bg"
											value="color_2" id="sidebar_bg_2"> <label
											for="sidebar_bg_2"></label>
									</span> <span> <input type="radio" name="sidebar_bg"
											value="color_3" id="sidebar_bg_3"> <label
											for="sidebar_bg_3"></label>
									</span> <span> <input type="radio" name="sidebar_bg"
											value="color_4" id="sidebar_bg_4"> <label
											for="sidebar_bg_4"></label>
									</span> <span> <input type="radio" name="sidebar_bg"
											value="color_5" id="sidebar_bg_5"> <label
											for="sidebar_bg_5"></label>
									</span> <span> <input type="radio" name="sidebar_bg"
											value="color_6" id="sidebar_bg_6"> <label
											for="sidebar_bg_6"></label>
									</span> <span> <input type="radio" name="sidebar_bg"
											value="color_7" id="sidebar_bg_7"> <label
											for="sidebar_bg_7"></label>
									</span> <span> <input type="radio" name="sidebar_bg"
											value="color_8" id="sidebar_bg_8"> <label
											for="sidebar_bg_8"></label>
									</span></li>
								</ul></li>
							<!--切换主题配色-->
						</ul>

					</div>
				</nav>

			</header>
			<!--End 头部信息-->

			<!--页面主要内容-->
			<main class="lyear-layout-content">

				<div id="iframe-content"></div>

			</main>
			<!--End 页面主要内容-->
		</div>
	</div>
	<div id="changePasswordModal" class="modal fade bs-example-modal-sm"
		tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title">修改密码</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon">原密码</span> <input id="password1"
									type="password" class="form-control" placeholder="请输入原密码"
									maxlength="30">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon">新密码</span> <input id="password2"
									type="password" class="form-control" placeholder="请输入新密码"
									maxlength="30">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="input-group">
								<span class="input-group-addon">确认密码</span> <input
									id="password3" type="password" class="form-control"
									placeholder="请确认新密码" maxlength="30">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" id="changeBtn" class="btn btn-primary">点击保存</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/static/plugins/lightyear/js/jquery.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/static/plugins/layer/layer.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/static/js/common.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/static/plugins/lightyear/js/bootstrap.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/static/plugins/lightyear/js/perfect-scrollbar.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/static/plugins/lightyear/js/bootstrap-multitabs/multitabs.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath }/static/plugins/lightyear/js/index.min.js"></script>
	<script type="text/javascript">
			//退出系统
			function logOut(){
				myLayer.confirm("确定退出系统？",function(){
					location.href = "${pageContext.request.contextPath }/admin/logout";
				});
			}

			//提交修改密码
			$('#changeBtn').click(function(){
				var password1 = $.trim($('#password1').val());
				var password2 = $.trim($('#password2').val());
				var password3 = $.trim($('#password3').val());
				if(password1 == null || password1 == undefined || password1.length == 0 ||
				   password2 == null || password2 == undefined || password2.length == 0 ||
				   password3 == null || password3 == undefined || password3.length == 0){
					myLayer.errorMsg("信息必须全部填写！！");
					return false;
				}
				if(!/^[a-z0-9A-Z]{5,15}$/.test(password2)){
					layer.errorMsg("密码由5-15位的字母数字组成！！");
					return false;
				}
				if(password2 != password3){
					myLayer.errorMsg("两次输入的密码不一致！！");
					return false;
				}
				if(password1 != '${admin.password}'){
					myLayer.errorMsg("原密码错误！！");
					return false;
				}
				$.post("${pageContext.request.contextPath }/admin/changePwd",{password:password2},function(data){

					if(data.flag){
						myLayer.successAlert(data.msg);
						setTimeout(function(){
							location.href = "${pageContext.request.contextPath }/app/login";
						},1000);
					}else{
						myLayer.errorMsg(data.msg);
					}
				});
			});
		</script>
</body>
</html>
