<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
		<title>客户信息管理</title>
		<link rel="icon" href="${pageContext.request.contextPath }/static/plugins/lightyear/favicon.ico" type="image/ico">
		<meta name="keywords" content="客户信息管理">
		<meta name="description" content="客户信息管理">
		<link href="${pageContext.request.contextPath }/static/css/common.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath }/static/plugins/lightyear/css/bootstrap.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath }/static/plugins/lightyear/css/materialdesignicons.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath }/static/plugins/lightyear/css/animate.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath }/static/plugins/lightyear/css/style.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath }/static/plugins/lightyear/js/bootstrap-table/bootstrap-table.min.css"
		 rel="stylesheet">
	</head>

	<body>
		<div class="container-fluid p-t-15">

			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header">
							<h4>客户管理</h4>
						</div>
						<div class="card-body">

							<div id="toolbar" class="toolbar-btn-action">
								<button id="btn_add" type="button" class="btn btn-primary m-r-5">
									<span class="mdi mdi-plus" aria-hidden="true"></span>新增
								</button>
								<button id="btn_delete" type="button" class="btn btn-danger">
									<span class="mdi mdi-window-close" aria-hidden="true"></span>删除
								</button>
								<button id="btn_restart" type="button" class="btn btn-info">
									<span class="mdi mdi-restart" aria-hidden="true"></span>
									<a href="javascript:location.reload();" style="text-decoration: none;color: white;">刷新</a>
								</button>
							</div>
							<table id="tb_departments"></table>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!--新增操作model-->
		<div class="modal fade" id="addModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="border-radius: 5px;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="">新增客户</h4>
					</div>
					<div class="modal-body">
						<form id="addForm" action="" method="POST">
							<div class="row">
								<div class="col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="mdi mdi-account-circle"></i>用户名</span>
										<input id="addUsername" type="text" name="username" class="form-control" placeholder="请输入用户名" maxlength="30">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="mdi mdi-lock"></i>密码</span>
										<input id="addPassword" type="password" name="password" class="form-control" placeholder="请输入密码" maxlength="30">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="mdi mdi-rename-box"></i>名称</span>
										<input id="addName" type="text" name="name" class="form-control" placeholder="例如：小明" maxlength="10">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="input-group">
										<span class="input-group-addon">余额</span>
										<input id="addMoney" type="text" name="money" class="form-control" placeholder="0.0" maxlength="15">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="mdi mdi-email"></i> 邮箱</span>
										<input id="addEmail" type="text" name="email" class="form-control" placeholder="xxx@xx.xxx" maxlength="30">
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer" class="form-control">
						<button type="button" id="modal_add_btn" class="btn btn-success">提交</button>
						<button type="button" class="btn btn-warning" data-dismiss="modal">取消</button>
					</div>
				</div>
			</div>
		</div>

		<!-- 编辑客户model -->
		<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="border-radius: 5px;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="updateLabel">编辑客户</h4>
					</div>
					<div class="modal-body">
						<form id="updateForm" action="" method="POST">
							<input type="hidden" id="updateId" name="id">
							<div class="row">
								<div class="col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="mdi mdi-account-circle"></i>用户名</span>
										<input id="updateUsername" disabled="disabled" type="text" name="username" class="form-control" placeholder="请输入用户名" maxlength="30">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="mdi mdi-lock"></i>密码</span>
										<input id="updatePassword" type="password" name="password" class="form-control" placeholder="请输入密码" maxlength="30">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="mdi mdi-rename-box"></i>名称</span>
										<input id="updateName" type="text" name="name" class="form-control" placeholder="例如：小明" maxlength="10">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="input-group">
										<span class="input-group-addon">余额</span>
										<input id="updateMoney" type="text" name="money" class="form-control" placeholder="0.0" maxlength="15">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="input-group">
										<span class="input-group-addon"><i class="mdi mdi-email"></i> 邮箱</span>
										<input id="updateEmail" type="text" name="email" class="form-control" placeholder="xxx@xx.xxx" maxlength="30">
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" id="modal_update_btn" class="btn btn-success">提交</button>
						<button type="button" class="btn btn-warning" data-dismiss="modal">取消</button>
					</div>
				</div>
			</div>
		</div>
		</div>


		<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/lightyear/js/jquery.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/layer/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/lightyear/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/lightyear/js/perfect-scrollbar.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/lightyear/js/bootstrap-table/bootstrap-table.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/lightyear/js/bootstrap-table/bootstrap-table-zh-CN.min.js"></script>

		<!--行内编辑插件-->
		<link href="${pageContext.request.contextPath }/static/plugins/lightyear/js/x-editable/1.5.1/bootstrap3-editable/css/bootstrap-editable.min.css"
		 rel="stylesheet">
		<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/lightyear/js/x-editable/1.5.1/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/lightyear/js/bootstrap-table/extensions/editable/bootstrap-table-editable.min.js"></script>

		<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/lightyear/js/main.min.js"></script>
		<script type="text/javascript">
			//初始化数据表格
			$('#tb_departments').bootstrapTable({
				classes: 'table table-bordered table-hover table-striped',
				url: '${pageContext.request.contextPath }/user/findAll',
				method: 'get',
				dataType: 'json', // 因为本示例中是跨域的调用,所以涉及到ajax都采用jsonp,
				uniqueId: 'id',
				idField: 'id', // 每行的唯一标识字段
				toolbar: '#toolbar', // 工具按钮容器
				clickToSelect: false, // 是否启用点击选中行
				showColumns: true, // 是否显示所有的列
				showRefresh: true, // 是否显示刷新按钮

				//showToggle: true,        // 是否显示详细视图和列表视图的切换按钮(clickToSelect同时设置为true时点击会报错)

				pagination: true, // 是否显示分页
				sortOrder: "asc", // 排序方式
				queryParams: function(params) {
					var temp = {
						pageSize: params.limit, // 每页数据量
						offset: params.offset, // sql语句起始索引
						page: (params.offset / params.limit) + 1,
						sort: params.sort, // 排序的列名
						sortOrder: params.order, // 排序方式'asc' 'desc'
						keyWord: params.search,
					};
					return temp;
				}, // 传递参数
				sidePagination: "server", // 分页方式：client客户端分页，server服务端分页
				pageNumber: 1, // 初始化加载第一页，默认第一页
				pageSize: 5, // 每页的记录行数
				pageList: [5, 10, 25, 50], // 可供选择的每页的行数
				search: true, // 是否显示表格搜索，此搜索是客户端搜索
				//showExport: true,        // 是否显示导出按钮, 导出功能需要导出插件支持(tableexport.min.js)
				//exportDataType: "basic", // 导出数据类型, 'basic':当前页, 'all':所有数据, 'selected':选中的数据

				columns: [{
					checkbox: true // 是否显示复选框
				}, {
					field: 'id',
					title: 'ID',
					sortable: true // 是否排序
				}, {
					field: 'username',
					title: '用户名',
					sortable: true
				}, {
					field: 'name',
					title: '名称',
					sortable: true
				}, {
					field: 'money',
					title: '余额',
					sortable: true
				}, {
					field: 'email',
					title: '邮箱',
					sortable: true
				}, {
					field: 'createTimeStr',
					title: '创建时间'
				}, {
					field: 'operate',
					title: '操作',
					formatter: btnGroup, // 自定义方法
					events: {
						'click .edit-btn': function(event, value, row, index) {
							editUser(row.id);
						},
						'click .del-btn': function(event, value, row, index) {
							del(row.id);
						}
					}
				}],
				onLoadSuccess: function(data) {
					$("[data-toggle='tooltip']").tooltip();
				}
			});

			// 操作按钮
			function btnGroup() {
				let html =
					'<a href="#!" class="btn btn-xs btn-default m-r-5 edit-btn" title="编辑" data-toggle="tooltip" value="edit"><i class="mdi mdi-pencil"></i></a>' +
					'<a href="#!" class="btn btn-xs btn-default del-btn" title="删除" data-toggle="tooltip"><i class="mdi mdi-window-close"></i></a>';
				return html;
			}

			// 操作方法 - 编辑
			function editUser(id) {
				//通过id查找用户信息，并渲染 编辑模态框
				$.get('${pageContext.request.contextPath}/user/findById', {
					id: id
				}, function(data) {
					//设置编辑模态框默认值
					$("#updateId").val(data.id);
					$("#updateUsername").val(data.username);
					$("#updateName").val(data.name);
					$("#updatePassword").val(data.password);
					$("#updateEmail").val(data.email);
					$("#updateMoney").val(data.money);
					$("#updateModal").modal('show');
				});
			}
			// 操作方法 - 删除单个
			function del(id) {
				myLayer.confirm("确定删除这条记录？", function() {
					$.get('${pageContext.request.contextPath}/user/del', {
						id: id
					}, function(data) {
						
						if (data.flag) {
							myLayer.successMsg(data.msg);
							setTimeout(function() {
								location.reload();
							}, 1000);
						} else {
							myLayer.errorMsg(data.msg);
						}
					});
				});
			}

			//数据校验
			function checkData(data) {
				var username = data.username;
				var password = data.password;
				var username = data.username;
				var name = data.name;
				var email = data.email;
				var money = data.money;
				if (username == null || username.length == 0 || username == undefined) {
					myLayer.errorMsg("用户名必须填写！！");
					return false;
				}
				if (!/^[A-Z][a-z0-9A-Z]{4,14}$/.test(username)) {
					myLayer.errorMsg("用户名必须大写字母开头，5-15位！！");
					return false;
				}
				if (password == null || password.length == 0 || password == undefined) {
					myLayer.errorMsg("密码必须填写！！");
					return false;
				}
				if (!/^[a-z0-9A-Z]{4,15}$/.test(password)) {
					myLayer.errorMsg("密码由5-15位的字母数字组成！！");
					return false;
				}
				if (name == null || name.length == 0 || name == undefined) {
					myLayer.errorMsg("名称必须填写！！");
					return false;
				}
				if (!name.length >= 3 && name.length <= 10) {
					myLayer.errorMsg("名称的长度为3-10！！");
					return false;
				}
				if (email == null || email.length == 0 || email == undefined) {
					myLayer.errorMsg("邮箱必须填写！！");
					return false;
				}
				if (!/^\w+@\w+(\.\w+){1,3}$/i.test(email)) {
					myLayer.errorMsg("邮箱格式不正确！！");
					return false;
				}
				if (!/^0|([1-9][0-9]{1,14})$/.test(money)) {
					myLayer.errorMsg("余额必须是15位内的数字！！");
					return false;
				}
				return true;
			}
			//提交修改后的信息
			$("#modal_update_btn").click(function() {
				var username = $.trim($("#updateUsername").val());
				var password = $.trim($("#updatePassword").val());
				var name = $.trim($("#updateName").val());
				var money = $.trim($("#updateMoney").val());
				var email = $.trim($("#updateEmail").val());
				//输入验证：
				var data = {
					username: username,
					password: password,
					name: name,
					money: money,
					email: email
				};
				if (checkData(data)) {
					$.post('${pageContext.request.contextPath }/user/update', $('#updateForm').serialize(), function(data) {
						
						if (data.flag) {
							myLayer.successMsg(data.msg);
							setTimeout(function() {
								location.reload();
							}, 1000);
						} else {
							myLayer.errorMsg(data.msg);
							return false;
						}
					});
				}
			});

			//批量删除
			$("#btn_delete").click(function() {
				var selects = $('#tb_departments').bootstrapTable('getSelections');
				if (selects.length == 0) {
					myLayer.errorMsg("请选择需要删除的记录！！");
					return false;
				}
				var ids = "";
				$(selects).each(function() {
					ids += this.id + ",";
				});
				ids = ids.substring(0, ids.length - 1);
				myLayer.confirm("确定删除选中的记录？", function() {
					$.get("${pageContext.request.contextPath}/user/delete", {
						ids: ids
					}, function(data) {
						
						if (data.flag) {
							myLayer.successMsg(data.msg);
							setTimeout(function() {
								location.reload();
							}, 1000);
						} else {
							myLayer.errorMsg(data.msg);
						}
					});
				})
			});

			//点击增添按钮清空原表单数据
			$("#btn_add").click(function() {
				$("#addUsername").val("");
				$("#addPassword").val("");
				$("#addName").val("");
				$("#addMoney").val("");
				$("#addEmail").val("");
				//显示数据
				$("#addModel").modal('show');
			});

			//提交增加数据
			$("#modal_add_btn").click(function() {
				var username = $.trim($("#addUsername").val());
				var password = $.trim($("#addPassword").val());
				var name = $.trim($("#addName").val());
				var money = $.trim($("#addMoney").val());
				var email = $.trim($("#addEmail").val());
				if (money == null || money.length == 0 || money == undefined) {
					$("#addMoney").val("0");
				}
				//输入验证：
				var data = {
					username: username,
					password: password,
					name: name,
					money: money,
					email: email
				};
				if (checkData(data)) {
					$.post("${pageContext.request.contextPath }/user/findByUsername", {
						username: username
					}, function(data) {
						if (data != null && data != "null" && data != "") {
							myLayer.errorMsg("该用户名已被使用！！");
							return false;
						} else {
							$.post('${pageContext.request.contextPath }/user/add', $('#addForm').serialize(), function(data) {
								
								if (data.flag) {
									myLayer.successMsg(data.msg);
									setTimeout(function() {
										location.reload();
									}, 1000);
								} else {
									myLayer.errorMsg(data.msg);
								}
							});
						}
					});
				}
			});
		</script>
	</body>
</html>
