<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<title>请登录！！</title>
<link rel="icon" href="${pageContext.request.contextPath }/static/img/favicon.ico" type="image/ico">
<meta name="keywords" content="青青草在线书城系统">
<meta name="description" content="青青草在线书城系统">
<link href="${pageContext.request.contextPath }/static/css/bootstrap.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath }/static/css/materialdesignicons.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath }/static/css/style.min.css" rel="stylesheet">
<style>
body{
    background-color: #fff;
}
.error-page {
    height: 100%;
    position: fixed;
    width: 100%;
}
.error-body {
    padding-top: 5%;
}
.error-body h1 {
    font-size: 210px;
    font-weight: 700;
    text-shadow: 4px 4px 0 #f5f6fa, 6px 6px 0 #33cabb;
    line-height: 210px;
    color: #33cabb;
}
.error-body h4 {
    margin: 30px 0px;
}
</style>
</head>
  
<body>
<section class="error-page">
  <div class="error-box">
    <div class="error-body text-center">
      <h1>权限不够！！</h1>
      <h4>请登录后再进行操作！！</h4>
      <a href="${pageContext.request.contextPath}/app/login" class="btn btn-primary ">跳转登录</a>
    </div>
  </div>
</section>
<script type="text/javascript">
	setTimeout(function(){
		location.href = "${pageContext.request.contextPath}/app/login";
	},2000);
</script>
</body>
</html>