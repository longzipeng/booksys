<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<header>
	<nav>
		<ul>
			<a href="${pageContext.request.contextPath}/app/index"><li>首页</li></a>
			<a href="#" data-toggle="modal" data-target="#aboutModal"><li>关于系统</li></a>
			<a href="${pageContext.request.contextPath}/app/login"><li>登录</li></a>
			<a href="${pageContext.request.contextPath}/app/register"><li>注册</li></a>
		</ul>
	</nav>
</header>