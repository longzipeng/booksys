<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>青青草在线书城系统-登录</title>
		<%@include file="title.jsp" %>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/login&register.css">
	</head>
	<body>
		<!--Header-->
		<%@include file="header.jsp" %>
		<!--Main-->
		<main>
			<div class="container">
				<img class="login_bg" src="${pageContext.request.contextPath}/static/img/login.png">
				<form class="form" id="loginForm" action="${pageContext.request.contextPath}/user/login" method="post">
					<h3>青青草在线书城系统</h3>
					<p>登录</p>
					<input type="text" autofocus="autofocus" id="username" value name="username" placeholder="用户名/邮箱" required="required">
					<input type="password" id="password" name="password" value placeholder="密码" required="required">
					<div class="select">
						<select name="power" id="select" style="margin-bottom: 10px;">
							<option value="user">----用 户----</option>
							<option value="admin">----管理员----</option>
						</select>
					</div>
					<input type="text" placeholder="输入验证码" id="check" name="checkCode" required="required" style="width: 100px;display: inline-block;"/>
					<img id="checkCode" title="点击切换验证码" src="${pageContext.request.contextPath}/pages/comment/checkCode.jsp" style="display: inline-block;width:70px;height: 36px;vertical-align: middle;cursor: pointer;" >
					<input id="submit" type="submit" name="submit" value="登录">
				</form>
			</div>
		</main>

		<!--Footer-->
		<%@include file="footer.jsp" %>
		
		<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/lightyear/js/jquery.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/static/plugins/layer/layer.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/common.js"></script>
		<script>
			//切换验证码
			function switchCheckCode(){
				$("#checkCode").attr('src',"${pageContext.request.contextPath}/pages/comment/checkCode.jsp?"+Math.random());
			}
			switchCheckCode();
			$("#checkCode").click(switchCheckCode);
			
			// 根据登陆用户的权限切换发送请求
			$('#select').change(function(){
				console.log($(this).val());
				$('.form').attr('action',"${pageContext.request.contextPath}/" + $(this).val() + "/login")
			});
			
			//表单提交
			$('.form').submit(function(){
				var username = $.trim($('#username').val());
				var password = $.trim($('#password').val());
				var checkCode = $.trim($('#check').val());
				console.log(checkCode);
				if(username.length == null||username == undefined || username.length==0){
					myLayer.errorMsg("账号不能为空！！");
					return false;
				}
				if(password.length == null||password == undefined || password.length==0){
					myLayer.errorMsg("密码不能为空！！");
					return false;
				}
				if(checkCode.length == null||checkCode == undefined || checkCode.length==0){
					myLayer.errorMsg("验证码不能为空！！");
					return false;
				}
				var load = myLayer.load();
				$.post($(this).attr("action"),{username:username,password:password,checkCode:checkCode},function(data){
					
					if(data.flag){
						layer.close(load);
						myLayer.successAlert(data.msg);
						setTimeout(function(){
							if($("#select").val() == 'user'){
								location.href = "${pageContext.request.contextPath}/app/index";
							}else{
								location.href = "${pageContext.request.contextPath}/ad/index";
							}
						},1000);
					}else{
						layer.close(load);
						myLayer.errorAlert(data.msg);
						switchCheckCode();
					}
				});
				return false;
			});
		</script>
	</body>
</html>
