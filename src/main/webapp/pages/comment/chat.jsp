<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>青青草在线书城系统-联系我们</title>
<%@include file="title.jsp" %>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/static/css/login&register.css">
</head>
<body>
	<!--Header-->
	<%@include file="header.jsp"%>

	<!--Main-->
	<main>
	<div class="container">
		<!-- <div><h1>有问题请扫码加卑微小周微信！</h1></div> -->
		<img class="login_bg"
			src="${pageContext.request.contextPath}/static/img/chat.png">
	</div>
	</main>

	<!--Footer-->
	<%@include file="footer.jsp"%>
</body>
</html>