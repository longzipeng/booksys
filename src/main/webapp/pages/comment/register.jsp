<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>青青草在线书城系统-注册</title>
    <%@include file="title.jsp" %>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/static/css/register.css">
</head>
<body>
<!--Header-->
<%@include file="header.jsp" %>
<!--Main-->
<main>
    <div class="container">
        <img class="login_bg"
             src="${pageContext.request.contextPath}/static/img/login.png">
        <form id="register_form" class="register_form"
              action="${pageContext.request.contextPath}/user/register"
              method="post" required="required">
            <h3>青青草在线书城系统</h3>
            <p>注册</p>
            <input type="text" autofocus="autofocus" name="username"
                   id="username" placeholder="用户名" required="required"> <input
                type="password" name="password" id="password" placeholder="密码"
                required="required"> <input type="password"
                                            name="confirmPsd" id="repassword" placeholder="再次输入密码"
                                            required="required"> <input type="text" name="name"
                                                                        id="name" placeholder="姓名" required="required">
            <input
                    type="text" placeholder="请输入邮箱" name="email" id="email"
                    required="required"/> <input type="text" placeholder="输入验证码"
                                                 id="check" name="checkCode" required="required"
                                                 style="width: 100px; display: inline-block;"/> <img id="checkCode"
                                                                                                     title="点击切换验证码"
                                                                                                     src="${pageContext.request.contextPath}/pages/comment/checkCode.jsp"
                                                                                                     style="display: inline-block; width: 70px; height: 36px; vertical-align: middle; cursor: pointer;">
            <input id="submit" type="submit" name="submit" value="注册">
        </form>
    </div>
</main>

<!--Footer-->
<%@include file="footer.jsp" %>
<!--Footer-->
<script type="text/javascript"
        src="${pageContext.request.contextPath }/static/plugins/lightyear/js/jquery.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath }/static/plugins/layer/layer.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath }/static/js/common.js"></script>
<script type="text/javascript">
    //切换验证码
    function switchCheckCode() {
        $("#checkCode").attr(
            'src',
            "${pageContext.request.contextPath}/pages/comment/checkCode.jsp?"
            + Math.random());
    }

    switchCheckCode();
    $("#checkCode").click(switchCheckCode);

    //用户名实时校验
    var usernameFlag = false;
    var passwordFlag = false;
    var repasswordFlag = false;
    var emailFlag = false;
    var nameFlag = false;
    $('#username')
        .change(
            function () {
                var username = $.trim($(this).val());
                var that = $(this);
                if (username != null && username != undefined
                    && username.length != 0) {
                    usernameFlag = true;
                    <%--if (/^[A-Z][a-z0-9A-Z]{4,14}$/.test(username)) {--%>
                    <%--    $--%>
                    <%--        .post(--%>
                    <%--            "${pageContext.request.contextPath }/user/findByUsername",--%>
                    <%--            {--%>
                    <%--                username: username--%>
                    <%--            },--%>
                    <%--            function (data) {--%>
                    <%--                console.log(data)--%>
                    <%--                console.log(typeof data)--%>
                    <%--                if (data == null--%>
                    <%--                    || data == "null") {--%>
                    <%--                    that--%>
                    <%--                        .removeClass('err');--%>
                    <%--                    that--%>
                    <%--                        .addClass('success');--%>
                    <%--                    usernameFlag = true;--%>
                    <%--                } else {--%>
                    <%--                    that--%>
                    <%--                        .removeClass('success');--%>
                    <%--                    that--%>
                    <%--                        .addClass('err');--%>
                    <%--                    layer--%>
                    <%--                        .msg(--%>
                    <%--                            "该用户名已被占用！！",--%>
                    <%--                            {--%>
                    <%--                                time: 1000--%>
                    <%--                            });--%>
                    <%--                    usernameFlag = false;--%>
                    <%--                }--%>
                    <%--            });--%>
                    <%--} else {--%>
                    <%--    that.removeClass('success');--%>
                    <%--    that.addClass('err');--%>
                    <%--    usernameFlag = false;--%>
                    <%--    layer.msg("账号必须大写字母开头，5-15位！！", {--%>
                    <%--        time: 1000--%>
                    <%--    });--%>
                    <%--}--%>
                } else {
                    that.removeClass('success');
                    that.addClass('err');
                    usernameFlag = false;
                    layer.msg("用户名不能为空!!", {
                        time: 1000
                    });
                }
            });
    //密码实时校验
    $('#password').change(
        function () {
            var password = $.trim($(this).val());
            var that = $(this);
            if (password != null && password != undefined
                && password.length != 0) {
                if (/^[a-z0-9A-Z]{4,15}$/.test(password)) {
                    that.removeClass('err');
                    that.addClass('success');
                    passwordFlag = true;
                } else {
                    that.removeClass('success');
                    that.addClass('err');
                    passwordFlag = false;
                    layer.msg("密码由5-15位的字母数字组成！！", {
                        time: 1000
                    });
                }
            } else {
                that.removeClass('success');
                that.addClass('err');
                passwordFlag = false;
                layer.msg("密码不能为空!!", {
                    time: 1000
                });
            }
        });
    $('#repassword').change(
        function () {
            var password = $.trim($(this).val());
            var that = $(this);
            if (password != null && password != undefined
                && password.length != 0) {
                if (password == $('#password').val()) {
                    that.removeClass('err');
                    that.addClass('success');
                    repasswordFlag = true;
                } else {
                    that.removeClass('success');
                    that.addClass('err');
                    repasswordFlag = false;
                    layer.msg("两次输入的密码不一样！！", {
                        time: 1000
                    });
                }
            } else {
                that.removeClass('success');
                that.addClass('err');
                repasswordFlag = false;
                layer.msg("请确认密码!!", {
                    time: 1000
                });
            }
        });
    //邮箱格式验证
    $('#email').change(function () {
        var email = $.trim($(this).val());
        var that = $(this);
        if (email != null && email != undefined && email.length != 0) {
            if (/\w+@\w+(\.\w+){1,3}/i.test(email)) {
                that.removeClass('err');
                that.addClass('success');
                emailFlag = true;
            } else {
                that.removeClass('success');
                that.addClass('err');
                emailFlag = false;
                layer.msg("邮箱格式错误!!", {
                    time: 1000
                });
            }
        } else {
            that.removeClass('success');
            that.addClass('err');
            emailFlag = false;
            layer.msg("邮箱不能为空!!", {
                time: 1000
            });
        }
    });

    //名称验证
    $('#name').change(function () {
        var name = $.trim($(this).val());
        var that = $(this);
        if (name != null && name != undefined && name.length != 0) {
            if (name.length >= 3 && name.length <= 10) {
                that.removeClass('err');
                that.addClass('success');
                nameFlag = true;
            } else {
                that.removeClass('success');
                that.addClass('err');
                nameFlag = false;
                layer.msg("名称长度为3-10", {
                    time: 1000
                });
            }
        } else {
            that.removeClass('success');
            that.addClass('err');
            nameFlag = false;
            layer.msg("名称不能为空!!", {
                time: 1000
            });
        }
    });

    $('#register_form')
        .submit(
            function () {
                var load = myLayer.load();
                if (nameFlag && usernameFlag && passwordFlag
                    && repasswordFlag && emailFlag) {
                    $
                        .post(
                            '${pageContext.request.contextPath }/user/register',
                            $('#register_form').serialize(),
                            function (data) {

                                layer.close(load);
                                if (data.flag) {
                                    myLayer
                                        .successAlert(data.msg);
                                    setTimeout(
                                        function () {
                                            location.href = "${pageContext.request.contextPath }/app/login";
                                        }, 1000);
                                } else {
                                    myLayer
                                        .errorMsg(data.msg);
                                    switchCheckCode();
                                }
                            });
                } else {
                    layer.close(load);
                    myLayer.errorAlert("请检测信息是否完全正确！！");
                }
                return false;
            });
</script>
</body>
</html>