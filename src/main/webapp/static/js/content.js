function back() {
	window.history.back(-1);
}

function confirm() {
	var password = document.getElementsByName("password")[0].value;
	var confirmPsd = document.getElementsByName("confirmPsd")[0].value;
	if (password==confirmPsd) {
		return true;
	}else{
		alert("两次密码不一致，请重新输入！");
	}
	return false;
}

function checkName() {
	var username = document.getElementsByName("username")[0].value;
	var nameSpan = document.getElementById("nameSpan");
	var reg = /^[a-z]\w{1,17}$/i;
	if (reg.test(username)) {
		nameSpan.innerHTML = "✔".fontcolor("red");
		return true;
	}else{
		nameSpan.innerHTML = "✖".fontcolor("red");
	}
	return false;
}

function checkPsd() {
	var password = document.getElementsByName("password")[0].value;
	var psdSpan = document.getElementById("psdSpan");
	var reg = /^\w{6,20}$/i;
	if (reg.test(password)) {
		psdSpan.innerHTML = "✔".fontcolor("red");
		return true;
	}else{
		psdSpan.innerHTML = "✖".fontcolor("red");
	}
	return false; 
}

function checkPhone() {
	var phone = document.getElementsByName("phone")[0].value;
	var phoneSpan = document.getElementById("phoneSpan");
	var reg = /^1[345789]\d{9}$/i;
	if (reg.test(phone)) {
		phoneSpan.innerHTML = "✔".fontcolor("red");
		return true;
	}else{
		phoneSpan.innerHTML = "✖".fontcolor("red");
	}
	return false;
}

function checkForm() {
	if (checkName() && checkPsd() && checkPhone() && confirm()) {
		document.forms[0].submit();
	}
}
